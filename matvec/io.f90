subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
  use mpi
  use common_routines
  implicit none
  integer, intent(out) :: n_row, n_val
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer, allocatable, intent(inout) :: col_ind(:)
  real(8), allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  integer h, i, j, s_ind, e_ind
  integer n_col, temp
  integer n_val_l, row_ind
  real(8) real_p, img_p
  integer, allocatable :: row_temp(:)
  character(10000) :: oneline
  integer :: fi = 10
  logical flag
  integer :: MCW=MPI_COMM_WORLD, ierr

  write(*, *) 'Sequential text I/O routine'
  
  open(fi, file = fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo
  read(oneline, *) n_row, n_col, n_val_l

  if(n_val_l > huge(n_val)) then
     write(*, *) 'Number of nonzero elements per each process is more than maxmum value of int4'
     write(*, *) 'Sequential file reading routine does not support this file.'
     write(*, *) 'Please use parallel reading routine (option is "-t Bin_para")'
     write(*, *) 'Notice :: target file must be binary file in parallel reading routine.'
     call MPI_Abort(MCW, -1, ierr)
  endif

  n_val = int(n_val_l, 4)
  allocate(row_ptr(n_row+1), col_ind(n_val), val(n_val), row_temp(n_val))

  do i = 1, n_val
     read(fi, *) row_temp(i), col_ind(i), real_p
     val(i) = real(real_p, 8)
  enddo

  close(fi)

  call quicksort(row_temp, col_ind, val, n_val)
  
  row_ind = 1
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind) then
        row_ptr(j) = i
        row_ind = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     call quicksort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
  enddo
end subroutine read_mm_form

subroutine read_mm_bin_form(fname, n_row, n_val, val, row_ptr, col_ind)
  use mpi
  use common_routines
  !$ use omp_lib
  implicit none
  integer, intent(out) :: n_row, n_val
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer, allocatable, intent(inout) :: col_ind(:)
  real(8), allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  double precision :: time
  integer h, i, j, s_ind, e_ind
  integer n_val_l, row_ind
  integer n_col, temp
  integer, allocatable :: row_temp(:)
  character(10000) :: oneline
  integer :: fi = 10
  logical flag
  integer :: MCW=MPI_COMM_WORLD, ierr

  type st_read
     integer row, col
     real(8) val
  end type st_read
  type(st_read), allocatable :: read_buf(:)

  write(*, *) 'Sequential binary I/O routine'

  time = omp_get_wtime()
  open(fi ,file = fname, FORM='unformatted',ACCESS='stream')

  read(fi) n_row, n_col, n_val_l
  if(n_val_l > huge(n_val)) then
     write(*, *) 'Number of nonzero elements per each process is more than maxmum value of int4.'
     write(*, *) 'Sequential file reading routine does not support this file.'
     write(*, *) 'Please use parallel reading routine (option is "-t Bin_para")'
     call MPI_Abort(MCW, -1, ierr)
  endif

  n_val = int(n_val_l, 4)
  allocate(row_ptr(n_row+1), col_ind(n_val), val(n_val), row_temp(n_val))
  allocate(read_buf(n_val))
  
  read(fi) read_buf
  close(fi)
  time = omp_get_wtime() - time
  write(*, *) 'Finish reading.'

  do i = 1, n_val
     row_temp(i) = read_buf(i)%row
     col_ind(i)  = read_buf(i)%col
     ! val(i)      = read_buf(i)%val
     val(i) = real(read_buf(i)%val, 8)
  enddo
  
  call quicksort(row_temp, col_ind, val, n_val)

  row_ind = 1
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind) then
        row_ptr(j) = i
        row_ind = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     call quicksort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
     do h = row_ptr(i), row_ptr(i+1)-1
        if(col_ind(h) == i) exit
     enddo
     if(h == row_ptr(i+1)) then
        write(*, *) 'There are no diagonal entries', i, h, row_ptr(i), row_ptr(i+1)-1
        stop
     endif
  enddo
  
  ! write(*, *) 'Data size for reading is', line_size * n_val / 1000000, 'MByte, bandwidth is ', line_size * n_val / 1000000 / time, 'MByte/sec.'
  
end subroutine read_mm_bin_form

subroutine read_vec(fname, n_row, b)
  implicit none
  integer, intent(in) :: n_row
  real(8), intent(inout) :: b(:)
  character(1000), intent(in) :: fname
  integer i, dummy
  real(8) real_p, img_p
  integer :: fi = 11
  character(10000) :: oneline
  
  open(fi, file=fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo

  do i = 1, n_row
     read(fi, *) dummy, b(i)
     ! read(fi, *) b(i)
  enddo
  close(fi)
  
end subroutine read_vec

logical function found_tgtstrg(strg) result(flag_found)
  implicit none
  character(*), intent(in) :: strg
  integer i, j, offst, plc, head
  
  interface
     logical function found_nonchar(char_in) result(flag_char)
       implicit none
       character, intent(in) :: char_in
     end function found_nonchar
  end interface

  flag_found = .false.
  
  head = 1
  do while(.true.)
     offst = index(strg(head:), 'bin')
     ! write(*, *) 'Check', head, offst, head + offst - 1, index(strg(head:), 'bin')
     if(offst /= 0) then
        plc = head + offst - 1
        if(found_nonchar(strg(plc-1:plc-1)) .and. found_nonchar(strg(plc+3:plc+3))) then
           flag_found = .true.
           exit
        else
           head = head + offst + 2
        endif
     else
        exit
     endif
  enddo

  head = 1
  do while(.true.)
     offst = index(strg(head:), 'BIN')
     ! write(*, *) 'Check', head, offst, head + offst - 1, index(strg(head:), 'bin')
     if(offst /= 0) then
        plc = head + offst - 1
        if(found_nonchar(strg(plc-1:plc-1)) .and. found_nonchar(strg(plc+3:plc+3))) then
           flag_found = .true.
           exit
        else
           head = head + offst + 2
        endif
     else
        exit
     endif
  enddo

  head = 1
  do while(.true.)
     offst = index(strg(head:), 'Bin')
     ! write(*, *) 'Check', head, offst, head + offst - 1, index(strg(head:), 'bin')
     if(offst /= 0) then
        plc = head + offst - 1
        if(found_nonchar(strg(plc-1:plc-1)) .and. found_nonchar(strg(plc+3:plc+3))) then
           flag_found = .true.
           exit
        else
           head = head + offst + 2
        endif
     else
        exit
     endif
  enddo

end function found_tgtstrg

logical function found_nonchar(char_in) result(flag_char)
  implicit none
  character, intent(in) :: char_in

  flag_char = .true.
  
  if((iachar('a') <= iachar(char_in) .and. iachar(char_in) <= iachar('z')) .or. &
     (iachar('A') <= iachar(char_in) .and. iachar(char_in) <= iachar('Z'))) flag_char = .false.
  
end function found_nonchar
  
