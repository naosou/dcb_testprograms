subroutine matvec(matA, x, y, temp_send, req, row_start, th_st, th_ed, th_st_send, th_ed_send, me_proc, num_procs)
  use mod_matvec
  use mpi
  !$ use omp_lib
  implicit none
  integer, intent(in) :: row_start, th_st, th_ed, th_st_send, th_ed_send, me_proc, num_procs
  integer, intent(inout) :: req(:)
  real(8), intent(in) :: x(:)
  real(8), intent(out) :: y(row_start:)
  real(8), intent(inout) :: temp_send(:)
  type(CRS_mat), intent(in) :: matA
  integer i, j, count, proc
  integer :: MCW=MPI_COMM_WORLD, st(MPI_STATUS_SIZE), ierr

  do i = th_st_send, th_ed_send
     temp_send(i) = x(matA%list_src(i))
  enddo
  !$OMP barrier

  !$OMP master
  count = 1
  do proc = 1, num_procs
     if(proc /= me_proc .and. matA%num_send(proc) /= 0) then
        i = matA%ptr_send(proc)
        call MPI_Isend(temp_send(i), matA%num_send(proc), MPI_DOUBLE_PRECISION, proc-1, 0, MCW, req(count), ierr)
        count = count + 1
     endif
  enddo
  do proc = 1, num_procs
     if(proc /= me_proc .and. matA%num_recv(proc) /= 0) then
        call MPI_recv(x(matA%ptr_recv(proc)), matA%num_recv(proc), MPI_DOUBLE_PRECISION, proc-1, 0, MCW, st, ierr)
     endif
  enddo
  !$OMP end master
  !$OMP barrier

  do j = th_st, th_ed
     y(j+row_start-1) = 0.0d0
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        y(j+row_start-1) = y(j+row_start-1) + matA%val(i) * x(matA%col_ind(i))
     enddo
  enddo

  !$OMP master
  call MPI_Waitall(count-1, req, MPI_STATUSES_IGNORE, ierr)
  !$OMP end master
  !$OMP barrier
  
end subroutine matvec
