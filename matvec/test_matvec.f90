program main
  use mod_matvec
  !$ use omp_lib
  use mpi
  use vtlb
  implicit none
  integer s_len, i_flag, ftype_A, ftype_B
  integer data_type, data_type_getc, bsize, ind_row, type_para
  integer n_row, n_val, i, j, k, thrd, bnd_min, bnd_max, status_ord, n_len, icolor, len_vec, status
  integer itr, maxitrs, size_thrd, itr_max_diff
  integer me_thrd, num_thrds, th_st_send, th_ed_send
  integer err_vtlb
  ! integer li, n_row_glb, n_val_glb
  integer :: temp_size(1), fo = 10, size_blk_vec=4
  integer, allocatable :: s_count(:), s_disp(:)
  real(8), allocatable :: b(:), x(:), r(:), b_temp(:)
  ! type(c_ptr) :: c_row_ptr, c_col_ind, c_val
  real(8) avg, norm, max_diff
  real(8) :: threashold = 1.0d-1
  type(CRS_mat) :: matA, matA_base
  type(BCRS_mat) :: block_matA
  real(8) st_time, ed_time, st_time2, ed_time2, st_time3, ed_time3, st_timew, ed_timew
  real(8), allocatable :: time_local(:, :), time_max(:)
  !real(c_double), allocatable, target :: val(:)
  logical flag_low, flag_up, shift_flag, flag_bin_A, flag_bin_B
  character(1000) :: fname_A, fname_B, strg
  real(8), allocatable :: temp_send(:)
  integer, allocatable :: row_start(:), row_end(:), thrd_start(:), thrd_end(:)
  integer, allocatable :: num_nonzero(:)
  integer :: me_proc, num_procs, proc, ierr, req_thrd=MPI_THREAD_FUNNELED, prov_thrd, MCW=MPI_COMM_WORLD
  integer, allocatable :: req(:)

  interface
     subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
       integer, intent(out) :: n_row, n_val
       integer, allocatable, intent(inout) :: row_ptr(:)
       integer, allocatable, intent(inout) :: col_ind(:)
       real(8), allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
     end subroutine read_mm_form

     subroutine read_mm_bin_form(fname, n_row, n_val, val, row_ptr, col_ind)
       integer, intent(out) :: n_row, n_val
       integer, allocatable, intent(inout) :: row_ptr(:)
       integer, allocatable, intent(inout) :: col_ind(:)
       real(8), allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
     end subroutine read_mm_bin_form

     subroutine check_matrix(row_ptr, col_ind, n_row, flag_low, flag_up)
       integer, intent(inout) :: row_ptr(:)
       integer, intent(inout) :: col_ind(:)
       integer, intent(in) :: n_row
       logical, intent(out) :: flag_low, flag_up
     end subroutine check_matrix

     subroutine move_low_to_up(row_ptr, col_ind, val, n_row, n_val)
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer, allocatable, intent(inout) :: col_ind(:)
       real(8),   allocatable, intent(inout) :: val(:)
       integer, intent(in) :: n_row, n_val
     end subroutine move_low_to_up

     subroutine expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
       implicit none
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer, allocatable, intent(inout) :: col_ind(:)
       real(8),   allocatable, intent(inout) :: val(:)
       integer, intent(inout) :: n_row, n_val
     end subroutine expand_up_to_low_wv

     subroutine communicate_crs(row_ptr, col_ind, val, n_row, row_start, row_end)
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer, allocatable, intent(inout) :: col_ind(:)
       real(8),   allocatable, intent(inout) :: val(:)
       integer, intent(in) :: n_row
       integer, intent(in) :: row_start(:), row_end(:)
     end subroutine communicate_crs

     subroutine calc_range_balance(num_list, n_row, start, end, num_para)
       integer, intent(in)  :: num_list(:), n_row, num_para
       integer, intent(out) :: start(:), end(:)
     end subroutine calc_range_balance

     subroutine check_balance(matA, thrd_start, thrd_end)
       use mod_matvec
       type(CRS_mat), intent(in) :: matA
       integer, intent(in) :: thrd_start, thrd_end
     end subroutine check_balance

     logical function found_tgtstrg(strg) result(flag_found)
       character(*), intent(in) :: strg
     end function found_tgtstrg

     subroutine crs_comp_halo(CRS, row_start, row_end, mpi_comm_part)
       use mod_matvec
       implicit none
       integer, intent(in) :: mpi_comm_part
       integer, intent(in) :: row_start(:), row_end(:)
       type(CRS_mat), intent(inout) :: CRS
     end subroutine crs_comp_halo

     subroutine matvec(matA, x, y, temp_send, req, row_start, th_st, th_ed, th_st_send, th_ed_send, me_proc, num_procs)
       use mod_matvec
       integer, intent(in) :: row_start, th_st, th_ed, th_st_send, th_ed_send, me_proc, num_procs
       integer, intent(inout) :: req(:)
       real(8), intent(in) :: x(:)
       real(8), intent(out) :: y(row_start:)
       real(8), intent(inout) :: temp_send(:)
       type(CRS_mat), intent(in) :: matA
     end subroutine matvec

  end interface

  call MPI_Init_thread(req_thrd, prov_thrd, ierr)
  st_timew = omp_get_wtime()
  call MPI_Comm_size(MCW ,num_procs ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1
  if(me_proc == 1) then
     write(*, *) 'init_thread', req_thrd, prov_thrd
     ! num_thrds = 1
     !$ num_thrds = omp_get_max_threads()
     write(*, *) 'Num_procs =', num_procs, 'Num_thrds =', num_thrds
  endif

  call vtlb_init_f(MCW, err_vtlb)
  
  if(command_argument_count() == 3) then
     call get_command_argument(1, fname_A)
     ! call get_command_argument(2, fname_B)
     call get_command_argument(2, strg)
     read(strg, *) type_para
     call get_command_argument(3, strg)
     read(strg, *) maxitrs
  else
     write(*, *) 'Command argument must be 2 or 4 as exec fname_mat, ftype_mat, fname_vec, ftype_vec'
     call MPI_Abort(MCW, -1, ierr)
  endif

  flag_bin_A = found_tgtstrg(fname_A)
  ! flag_bin_B = found_tgtstrg(fname_B)

  if(me_proc == 1) then
     st_time2 = omp_get_wtime()
     if(flag_bin_A) then
        call read_mm_bin_form(fname_A, n_row, n_val, matA_base%val, matA_base%row_ptr, matA_base%glb_col_ind)
     else
        call read_mm_form(fname_A, n_row, n_val, matA_base%val, matA_base%row_ptr, matA_base%glb_col_ind)
     endif

     call check_matrix(matA_base%row_ptr, matA_base%glb_col_ind, n_row, flag_low, flag_up)

     if(flag_up .and. (.not. flag_low)) then
        write(*, *) 'This matrix has only upper part.'
        call expand_up_to_low_wv(matA_base%row_ptr, matA_base%glb_col_ind, matA_base%val, n_row, n_val)
        ! deallocate(glb_matA_base%glb_col_ind, val)
     else if(flag_low .and. (.not. flag_up)) then
        write(*, *) 'This matrix has only lower part.'
        call move_low_to_up(matA_base%row_ptr, matA_base%glb_col_ind, matA_base%val, n_row, n_val)
        call expand_up_to_low_wv(matA_base%row_ptr, matA_base%glb_col_ind, matA_base%val, n_row, n_val)
        ! stop
     else
        write(*, *) 'Symmetric'
     endif
  endif

  call MPI_Barrier(MCW, ierr)

  call MPI_Bcast(n_row, 1, MPI_INTEGER, 0, MCW, ierr)
  ! n_row_glb = n_row

  !###################Decide ranges of each process#################################################################################
  allocate(row_start(num_procs), row_end(num_procs))
  if(type_para == 1) then
     if(me_proc == 1) write(*, *) "Even separation"
     do proc = 1, num_procs
        call calc_range(n_row, num_procs, proc, row_start(proc), row_end(proc))
     enddo
  else
     if(me_proc == 1) then
        write(*, *) "Balanced separation"
        allocate(num_nonzero(n_row))
        do i = 1, n_row
           num_nonzero(i) = matA_base%row_ptr(i+1) - matA_base%row_ptr(i)
        enddo
        call calc_range_balance(num_nonzero, n_row, row_start, row_end, num_procs)
        deallocate(num_nonzero)
     endif
     call MPI_Bcast(row_start, num_procs, MPI_Integer, 0, MCW, ierr)
     call MPI_Bcast(row_end,   num_procs, MPI_Integer, 0, MCW, ierr)
  endif
  n_row = row_end(me_proc) - row_start(me_proc) + 1
  matA_base%n_row = n_row

  ! write(*, *) 'Test range', me_proc, rrange%row_start, rrange%row_end

  !##################################################################################################################################

  call MPI_Barrier(MCW, ierr)
  st_time2 = omp_get_wtime()
  if(.not. flag_bin_A) then
     call communicate_crs(matA_base%row_ptr, matA_base%glb_col_ind, matA_base%val, n_row, row_start, row_end)
  else !Parallel file reading
     ! call ppohsol_read_mm_bin_form_para(fname_A, n_row_glb, n_val_glb, row_start, row_end, val, matA_base%row_ptr, matA_base%glb_col_ind, MCW)
  endif
  call MPI_Barrier(MCW, ierr)
  ed_time2 = omp_get_wtime()
  !##############Finish reading matrix file##################################################################################################

  !##############Reading right-hand vector##########################################################################################
  ! if(flags_ftype%r_vec == 3) then
  !    if(me_proc == 1) then
  !       allocate(b_temp(n_row))
  !       n_len = len_trim(fname_B)
  !       write(*, *) 'Read right hand vector from file', fname_B(1:n_len)
  !       call read_vec(fname_B, n_row, b_temp)
  !    else
  !       allocate(b_temp(0))
  !    endif
  !    allocate(s_count(num_procs), s_disp(num_procs))
  !    do proc = 1, num_procs
  !       s_count(proc) = int(row_end(proc) - row_start(proc) + 1, 4)
  !       s_disp(proc)  = int(row_start(proc) - 1, 4)
  !    enddo
  !    call MPI_Scatterv(b_temp, s_count, s_disp, comm_def_type, b, int(row_end(me_proc)-row_start(me_proc)+1, 4), comm_def_type, 0, MCW, ierr)
  !    deallocate(b_temp)
  ! else
  ! b(:) = 0.0d0
  ! endif

  !##########################################################################################################################################

  call MPI_Barrier(MCW, ierr)
  st_time3 = omp_get_wtime()
  !ppohsol have factrized result and matrix A for solver
  !row_start and row_end has the range of stored matrix. The size of this array must be same number of processes.
  call crs_comp_halo(matA_base, row_start, row_end, MCW)
  ! call ppohsol_precon(row_ptr, col_ind, val, st_sol, flags_pcg, row_start, row_end, MCW, status)
  call MPI_Barrier(MCW, ierr)
  ed_time3 = omp_get_wtime()

  ! x(1:n_row) = 1.0d0
  ! x(:) = 1.0d0
  ! do i = 1, n_row
  !    x(i) = real(i, 16) * 1.0d-5
  ! enddo

  call vtlb_verbose_f("Before blancing", err_vtlb)
  call vtlb_balance_f(matA_base%row_ptr(matA_base%n_row+1)-1, err_vtlb)
  call vtlb_verbose_f("After  blancing", err_vtlb)
  
  num_thrds = omp_get_max_threads()
  allocate(thrd_start(num_thrds), thrd_end(num_thrds), num_nonzero(n_row))
  do i = 1, n_row
     num_nonzero(i) = matA_base%row_ptr(i+1) - matA_base%row_ptr(i)
  enddo
  call calc_range_balance(num_nonzero, n_row, thrd_start, thrd_end, num_thrds)
  deallocate(num_nonzero)

  allocate(temp_send(matA_base%num_send_all), req(num_procs))
  
  !###################Decide ranges of each process#################################################################################
  do proc = 1, num_procs
     if(proc == me_proc) then
        if(proc == 1) write(*, '(a)') 'Check range'
        write(*, *) 'proc =', me_proc, ', range =', row_start(me_proc), row_end(me_proc), ', thrd =', thrd_start(:), thrd_end(num_thrds)
     endif
     call MPI_Barrier(MCW, ierr)
  enddo
  ! write(*, *) 'Test range', me_proc, rrange%row_start, rrange%row_end
  !##################################################################################################################################

  !$OMP parallel default(none) shared(matA, matA_base, x, b, temp_send, req, row_start, row_end, thrd_start, thrd_end, time_local, &
  !$OMP                               MPI_IN_PLACE, MCW) &
  !$OMP                       private(thrd, size_thrd, me_thrd, num_thrds, th_st_send, th_ed_send, ierr, st_time, ed_time, itr, &
  !$OMP                               time_max, itr_max_diff, max_diff) &
  !$OMP                  firstprivate(me_proc, num_procs, maxitrs)
  
  num_thrds = omp_get_num_threads()
  me_thrd = omp_get_thread_num() + 1

  call calc_range(matA_base%num_send_all, num_thrds, me_thrd, th_st_send, th_ed_send)
  ! write(*, *) 'Check copy range', me_proc, me_thrd, matA_base%num_send_all, th_st_send, th_ed_send
  temp_send(th_st_send:th_ed_send) = 0.0d0

  !Must be longer than row_end(me_proc) - row_start(me_proc) + 1. st_sol%len_vec_sol is the best size.
  !$OMP master
  allocate(b(row_start(me_proc):row_end(me_proc)), x(matA_base%len_vec))
  !$OMP end master
  !$OMP barrier
  x(thrd_start(me_thrd):thrd_end(me_thrd)) = 1.0d-5
  b(row_start(me_proc)+thrd_start(me_thrd)-1:row_start(me_proc)+thrd_end(me_thrd)-1) = 0.0d0

  !$OMP master
  matA = matA_base
  allocate(time_local(num_thrds, maxitrs))
  write(*, *) 'Check params', me_proc, num_thrds
  !$OMP end master
  !$OMP barrier
  ! call reallocate(matA)
  
  call check_balance(matA, thrd_start(me_thrd), thrd_end(me_thrd))
  size_thrd = thrd_end(me_thrd) - thrd_start(me_thrd) + 1

  do itr = 1, 10
     call matvec(matA, x, b, temp_send, req, row_start(me_proc), thrd_start(me_thrd), thrd_end(me_thrd), th_st_send, th_ed_send, me_proc, &
                                                                                                                                       num_procs)
     ! x(thrd_start(me_thrd):thrd_end(me_thrd)) = b(row_start(me_proc)+thrd_start(me_thrd)-1:row_start(me_proc)+thrd_end(me_thrd)-1) * 1.0d-5
     x(thrd_start(me_thrd):thrd_end(me_thrd)) = 1.0d-5
     !$OMP barrier
  enddo

  !$OMP master
  call MPI_Barrier(MCW, ierr)
  !$OMP end master
  !$OMP barrier
  st_time = omp_get_wtime()
  do itr = 1, maxitrs
     time_local(me_thrd, itr) = omp_get_wtime()
     call matvec(matA, x, b, temp_send, req, row_start(me_proc), thrd_start(me_thrd), thrd_end(me_thrd), th_st_send, th_ed_send, me_proc, &
                                                                                                                                       num_procs)
     ! x(thrd_start(me_thrd):thrd_end(me_thrd)) = b(row_start(me_proc)+thrd_start(me_thrd)-1:row_start(me_proc)+thrd_end(me_thrd)-1) * 1.0d-5
     x(thrd_start(me_thrd):thrd_end(me_thrd)) = 1.0d-5
     time_local(me_thrd, itr) = omp_get_wtime() - time_local(me_thrd, itr)
     !$OMP barrier
  enddo
  !$OMP master
  call MPI_Barrier(MCW, ierr)
  !$OMP end master
  !$OMP barrier
  ed_time = omp_get_wtime()

  !$OMP master
  if(me_proc == 1) then
     write(*, *) 'Time per one matvec is ', (ed_time - st_time) / maxitrs
  endif
  allocate(time_max(maxitrs))
  do itr = 1, maxitrs
     time_max(itr) = 0.0d0
     do thrd = 1, num_thrds
        if(time_max(itr) < time_local(thrd, itr)) time_max(itr) = time_local(thrd, itr)
     enddo
  enddo
  call MPI_Allreduce(MPI_IN_PLACE, time_max, maxitrs, MPI_INTEGER, MPI_MAX, MCW, ierr)

  max_diff = 0.0d0
  do itr = 1, maxitrs
     if(max_diff < time_max(itr)) then
        max_diff = time_max(itr)
        itr_max_diff = itr
     endif
  enddo

  do proc = 1, num_procs
     if(proc == me_proc) then
        write(*, '(a, i4, a)', advance='no') 'Check balance time', me_proc, ' = '
        do thrd = 1, num_thrds
           write(*, '(f7.4)', advance='no') time_local(thrd, itr_max_diff)*1.0d3
        enddo
        write(*, *)
     endif
     call MPI_Barrier(MCW, ierr)
  enddo

  do proc = 1, num_procs
     if(proc == me_proc) then
        write(*, '(a, i4, a)', advance='no') 'Check balance calc', me_proc, ' = '
        do thrd = 1, num_thrds
           write(*, '(i7)', advance='no') matA%row_ptr(thrd_end(thrd)) - matA%row_ptr(thrd_start(thrd))
        enddo
        write(*, *)
     endif
     call MPI_Barrier(MCW, ierr)
  enddo

  !$OMP end master
  
  !$OMP end parallel

  call vtlb_free_f(err_vtlb)
  
  !Checking results
  norm = dot_product(b, b)
  ! norm = dot_product(x(1:n_row), x(1:n_row))
  call MPI_Allreduce(MPI_IN_PLACE, norm, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MCW, ierr)
  if(me_proc == 1) then
     write(*, *) 'Check result', sqrt(norm)
  endif
  
  ! call ppohsol_deallocate(st_sol)
  call MPI_Finalize(ierr)

end program main

subroutine check_matrix(row_ptr, col_ind, n_row, flag_low, flag_up)
  implicit none
  integer, intent(inout) :: row_ptr(:)
  integer, intent(inout) :: col_ind(:)
  integer, intent(in) :: n_row
  logical, intent(out) :: flag_low, flag_up
  integer i, j
  
  write(*, *) 'Checking matrix as a symmetric ....'
  flag_low = .false.
  flag_up  = .false.
  do j = 1, n_row
     do i = row_ptr(j), row_ptr(j+1)-1
        if(col_ind(i) < j) then
           flag_low = .true.
        endif
        if(col_ind(i) > j) then
           flag_up = .true.
           exit
        endif
     enddo
     if(flag_up .and. flag_low) exit
  enddo

end subroutine check_matrix

subroutine move_low_to_up(row_ptr, col_ind, val, n_row, n_val)
  implicit none
  integer,    allocatable, intent(inout) :: row_ptr(:)
  integer, allocatable, intent(inout) :: col_ind(:)
  real(8),   allocatable, intent(inout) :: val(:)
  integer, intent(in) :: n_row, n_val
  integer, allocatable :: n_col_ind(:), n_row_ptr(:)
  real(8), allocatable :: na_val(:)
  integer, allocatable :: a_count(:)
  integer i, j

  allocate(a_count(n_row), na_val(n_val), n_col_ind(n_val), n_row_ptr(n_row+1))
  a_count = 0

  do j = 1, n_row
     do i= row_ptr(j), row_ptr(j+1)-1
        a_count(col_ind(i)) = a_count(col_ind(i)) + 1
     enddo
  enddo

  n_row_ptr(1) = 1
  do i = 1, n_row
     n_row_ptr(i+1) = n_row_ptr(i) + a_count(i)
  enddo

  a_count = 0
  do j = 1, n_row
     do i= row_ptr(j), row_ptr(j+1)-1
        na_val(n_row_ptr(col_ind(i)) + a_count(col_ind(i))) = val(i)
        n_col_ind(n_row_ptr(col_ind(i)) + a_count(col_ind(i))) = j
        a_count(col_ind(i)) = a_count(col_ind(i)) + 1
     enddo
  enddo

  deallocate(val, col_ind, row_ptr)
  allocate(val(n_val), col_ind(n_val), row_ptr(n_val))
  val = na_val
  col_ind = n_col_ind
  row_ptr = n_row_ptr
  deallocate(na_val, n_col_ind, n_row_ptr)

end  subroutine move_low_to_up

subroutine expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
  implicit none
  integer,    allocatable, intent(inout) :: row_ptr(:)
  integer, allocatable, intent(inout) :: col_ind(:)
  real(8),   allocatable, intent(inout) :: val(:)
  integer, intent(inout) :: n_row, n_val
  integer i, j, k
  integer, allocatable :: count_elem(:), exp_row_ptr(:), exp_col_ind(:)
  real(8), allocatable :: exp_val(:)

  allocate(count_elem(n_row))
  count_elem = 0
  do j = 1, n_row
     count_elem(j) = count_elem(j) + row_ptr(j+1) - row_ptr(j)
     do i = row_ptr(j)+1, row_ptr(j+1)-1
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo

  n_val = (row_ptr(n_row+1)-1)*2-n_row
  allocate(exp_row_ptr(n_row+1), exp_col_ind(n_val), exp_val(n_val))
  exp_row_ptr(1) = 1
  do i = 2, n_row+1
     exp_row_ptr(i) = exp_row_ptr(i-1) + count_elem(i-1)
     count_elem(i-1) = 0
  enddo

  do j = 1, n_row
     k = row_ptr(j+1) - row_ptr(j)
     exp_col_ind(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = int(col_ind(row_ptr(j):row_ptr(j+1)-1), 4)
     exp_val(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = val(row_ptr(j):row_ptr(j+1)-1)
     do i = row_ptr(j)+1, row_ptr(j+1)-1
        exp_col_ind(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = j
        exp_val(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = val(i)
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo

  deallocate(val, row_ptr, col_ind)
  allocate(val(n_val), col_ind(n_val), row_ptr(n_row+1))
  row_ptr = exp_row_ptr
  col_ind = exp_col_ind
  val = exp_val

  deallocate(exp_row_ptr, exp_col_ind, exp_val)
  
end subroutine expand_up_to_low_wv

subroutine calc_range(n_row, num_procs, me_proc, min, max)
  implicit none
  integer, intent(in)  :: n_row
  integer, intent(in)  :: num_procs, me_proc
  integer, intent(out) :: min, max
  integer rem, num

  rem = mod(n_row, num_procs)
  num = (n_row-rem) / num_procs

  if(me_proc <= rem) then
     min = (me_proc-1) * (num + 1) + 1
     max = min + num 
  else
     min = (me_proc-1) * num + rem + 1
     max = min + num - 1
  endif

end subroutine calc_range

subroutine calc_range_balance(num_list, n_row, start, end, num_para)
  implicit none
  integer, intent(in)  :: num_list(:), n_row, num_para
  integer, intent(out) :: start(:), end(:)
  integer i, head
  integer, allocatable :: load_list(:)
  real(8) sum_list, avg_list, sum_part
  
  sum_list = sum(num_list)
  avg_list = sum_list / real(num_para, 8)

  head = 1
  do i = 1, num_para
     start(i) = head
     sum_part = 0
     do while(sum_part < avg_list)
        sum_part = sum_part + num_list(head)
        head = head + 1
        if(head > n_row) exit
     enddo
     end(i) = head - 1
  enddo
  end(num_para) = n_row

  allocate(load_list(num_para))
  do i = 1, num_para
     load_list(i) = sum(num_list(start(i):end(i)))
  enddo

  ! write(*, *) 'Check loads', load_list
     
end subroutine calc_range_balance

subroutine communicate_crs(row_ptr, col_ind, val, n_row, row_start, row_end)
  use mpi
  implicit none
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer, allocatable, intent(inout) :: col_ind(:)
  real(8), allocatable, intent(inout) :: val(:)
  integer, intent(in) :: n_row
  integer, intent(in) :: row_start(:), row_end(:)
  integer ptr_start, ptr_end, row_bstart, row_bend, ptr_bstart, ptr_bend, count_elem , count_belem
  integer, allocatable :: temp_row(:)
  integer, allocatable :: temp_col(:)
  real(8), allocatable :: temp_val(:)
  integer :: me_proc, num_procs, ierr
  integer :: st(MPI_STATUS_SIZE), MCW=MPI_COMM_WORLD
  integer i, send_fin, recv_fin, count, proc
  integer send_rem, recv_rem
  integer(MPI_INTEGER_KIND) num_send, limit_comm, num_recv
  
  call MPI_Comm_size(MCW ,num_procs ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  limit_comm = int(huge(num_send) * 0.9d0, MPI_INTEGER_KIND) !Limitting the size of reading
  if(me_proc == 1) write(*, *) 'Check the size of limit', limit_comm
  
  if(me_proc == 1) then
     count = 1
     do proc = 2, num_procs

        ptr_start = row_ptr(row_start(proc))
        ptr_end   = row_ptr(row_end(proc)+1)-1
        count_elem = ptr_end - ptr_start + 1

        ! write(*, *) 'root', row_end-row_start+2
        call MPI_Send(row_ptr(row_start(proc)), int(row_end(proc)-row_start(proc)+2, MPI_INTEGER_KIND), MPI_INTEGER, proc-1, 0, MCW, ierr)
     
        send_rem = count_elem
        do while(.true.)
           if(send_rem < limit_comm)then
              num_send = int(send_rem, MPI_INTEGER_KIND)
           else
              num_send = limit_comm
           endif

           call MPI_Send(col_ind(ptr_start), num_send, MPI_INTEGER, proc-1, 0, MCW, ierr)
           call MPI_Send(val(ptr_start), num_send, MPI_DOUBLE_PRECISION, proc-1, 0, MCW, ierr)
           
           send_rem = send_rem - num_send
           count = count + 1
           if(send_rem == 0) exit
           if(send_rem < 0) then
              write(*, *) 'Check in sending matA. This is the bug.', send_rem, proc
              stop
           endif
           ptr_start = ptr_start + num_send
        enddo

     enddo
     
     allocate(temp_row(row_end(me_proc)+1), temp_col(row_ptr(row_end(me_proc)+1)-1), temp_val(row_ptr(row_end(me_proc)+1)-1))
     temp_row(1:row_end(me_proc)+1) = row_ptr(1:row_end(me_proc)+1)
     temp_col(1:row_ptr(row_end(me_proc)+1)-1) = col_ind(1:row_ptr(row_end(me_proc)+1)-1)
     temp_val(1:row_ptr(row_end(me_proc)+1)-1) = val(1:row_ptr(row_end(me_proc)+1)-1)
     deallocate(row_ptr, col_ind, val)
     allocate(row_ptr(row_end(me_proc)+1), col_ind(temp_row(row_end(me_proc)+1)-1), val(temp_row(row_end(me_proc)+1)-1))
     row_ptr(1:row_end(me_proc)+1) = temp_row(1:row_end(me_proc)+1)
     col_ind(1:row_ptr(row_end(me_proc)+1)-1) = temp_col(1:row_ptr(row_end(me_proc)+1)-1)
     val(1:row_ptr(row_end(me_proc)+1)-1) = temp_val(1:row_ptr(row_end(me_proc)+1)-1)
     deallocate(temp_row, temp_col, temp_val)

  else

     allocate(row_ptr(n_row+1))
     call MPI_Recv(row_ptr, n_row+1, MPI_INTEGER, 0, 0, MCW, st, ierr)
     do i = n_row+1, 1, -1
        row_ptr(i) = row_ptr(i) - row_ptr(1) + 1
     enddo
     count_elem = row_ptr(n_row+1) - 1
     allocate(col_ind(count_elem), val(count_elem))
     ! write(*, *) 'Chil, recv1', me_proc, count_elem

     count = 1
     ptr_start = 1
     recv_rem = count_elem
     do while(.true.)
        if(recv_rem < limit_comm)then
           num_recv = int(recv_rem, MPI_INTEGER_KIND)
        else
           num_recv = limit_comm
        endif

        call MPI_Recv(col_ind(ptr_start), num_recv, MPI_INTEGER, 0, 0, MCW, st, ierr)
        call MPI_Recv(val(ptr_start), num_recv, MPI_DOUBLE_PRECISION, 0, 0, MCW, st, ierr)
        
        recv_rem = recv_rem - num_recv
        count = count +  1
        if(recv_rem == 0) exit
        if(recv_rem < 0) then
           write(*, *) 'Check in recving matA. This is the bug.', recv_rem, proc
           stop
        endif
        ptr_start = ptr_start + num_recv           
     enddo
     
  endif
  
end subroutine communicate_crs

subroutine check_balance(matA, thrd_start, thrd_end)
  use mod_matvec
  use mpi
  !$ use omp_lib
  implicit none
  type(CRS_mat), intent(in) :: matA
  integer, intent(in) :: thrd_start, thrd_end
  integer i, proc, thrd, nnz_thrd
  integer, save :: nnz_proc, nnz_proc_min, nnz_proc_avg, nnz_proc_max, nnz_thrd_min, nnz_thrd_avg, nnz_thrd_max, num_thrds_whole
  integer me_proc, me_thrd, num_procs, num_thrds
  integer ierr
  integer :: MCW=MPI_COMM_WORLD

  call MPI_Comm_size(MCW ,num_procs ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)

  num_thrds = omp_get_num_threads()
  me_thrd = omp_get_thread_num()

  nnz_thrd = matA%row_ptr(thrd_end+1) - matA%row_ptr(thrd_start)

  !$OMP master
  nnz_proc = 0.0d0
  nnz_thrd_min = nnz_thrd
  nnz_thrd_max = nnz_thrd
  !$OMP end master
  !$OMP barrier

  !$OMP critical
  nnz_proc = nnz_proc + nnz_thrd
  if(nnz_thrd_min > nnz_thrd) nnz_thrd_min = nnz_thrd
  if(nnz_thrd_max < nnz_thrd) nnz_thrd_max = nnz_thrd
  !$OMP end critical
  !$OMP barrier

  ! write(*, *) 'Check calc', me_proc, me_thrd, nnz_thrd, nnz_proc
  
  !$OMP master
  call MPI_Allreduce(num_thrds, num_thrds_whole, 1, MPI_INTEGER, MPI_SUM, MCW, ierr)

  call MPI_Allreduce(nnz_proc, nnz_proc_min, 1, MPI_INTEGER, MPI_MIN, MCW, ierr)
  call MPI_Allreduce(nnz_proc, nnz_proc_avg, 1, MPI_INTEGER, MPI_SUM, MCW, ierr)
  nnz_thrd_avg = nnz_proc_avg / num_thrds_whole
  nnz_proc_avg = nnz_proc_avg / num_procs
  call MPI_Allreduce(nnz_proc, nnz_proc_max, 1, MPI_INTEGER, MPI_MAX, MCW, ierr)

  call MPI_Allreduce(nnz_thrd, nnz_thrd_min, 1, MPI_INTEGER, MPI_MIN, MCW, ierr)
  call MPI_Allreduce(nnz_thrd, nnz_thrd_max, 1, MPI_INTEGER, MPI_MAX, MCW, ierr)

  if(me_proc == 1) then
     write(*, '(a, 3i10, a, f5.2, a)') 'Check balance among procs', nnz_proc_min, nnz_proc_avg, nnz_proc_max, ', min/max = ', (1.0d0 - real(nnz_proc_min, 8)/real(nnz_proc_max, 8))*100.0d0, '%'
     write(*, '(a, 3i10, a, f5.2, a)') 'Check balance among thrds', nnz_thrd_min, nnz_thrd_avg, nnz_thrd_max, ', min/max = ', (1.0d0 - real(nnz_thrd_min, 8)/real(nnz_thrd_max, 8))*100.0d0, '%'
  endif

  !$OMP end master
  !$OMP barrier

  
end subroutine check_balance
