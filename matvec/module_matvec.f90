module mod_matvec
  implicit none
  
  type CRS_mat
     ! integer, allocatable :: col_ind(:), glb_col_ind(:), row_ptr(:), glb_addr(:), row_ptr_halo(:)
     integer,    allocatable :: col_ind(:), row_ptr(:), row_ptr_halo(:)
     integer, allocatable :: glb_col_ind(:), glb_addr(:)
     integer n_row, len_vec, num_send_all
     integer :: size_blk_vec = 0
     integer  n_row_glb
     integer, allocatable :: list_src(:),  num_send(:), num_recv(:), ptr_send(:), ptr_recv(:)
     real(8), allocatable :: val(:),  diag(:), inv_sqrt(:)
  end type CRS_mat


  type BCRS_mat
     integer bsize_col, bsize_row, n_belem_row, n_row, padding, len_vec, len_vec_blk
     integer :: size_blk_vec = 0
     integer num_send_all, num_send_all_blk !To support asymmetric block size, we need num_recv_all
     integer n_belem_row_glb, n_row_glb
     integer,    allocatable :: row_ptr(:), row_ptr_halo(:), col_ind(:), col_bind(:)
     integer, allocatable :: glb_col_ind(:), glb_col_bind(:), glb_addr(:), glb_addr_blk(:)
     integer,    allocatable :: col_ind_list_roww(:), row_ind_list_roww(:), col_ind_list_colw(:), row_ind_list_colw(:), row_ptr_reod(:), &
                                num_outer(:), num_row(:), elems_ptr(:), incr_list(:), strd_list(:)
     integer, allocatable :: list_src(:),  num_send(:), num_recv(:), ptr_send(:), ptr_recv(:)
     integer, allocatable :: list_src_blk(:),  num_send_blk(:), num_recv_blk(:), ptr_send_blk(:), ptr_recv_blk(:)
     integer, allocatable :: sorted_order(:)
     real(8), allocatable :: val(:, :, :), val_tr(:, :, :), dgn(:, :, :), inv_dgn(:, :, :), inv_sqrt(:), inv_bsqrt(:, :, :)
     real(8), allocatable :: reod_val(:, :, :), reod_val_tr(:, :, :), reod_inv_dgn(:, :, :), reod_inv_dgn_tr(:, :, :)
  end type BCRS_mat

end module mod_matvec
