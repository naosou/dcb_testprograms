!############# Information of each value ##############
!
!row_ptr(i)          row_ptr_halo(i)    row_ptr(i+1)-1
!.....|---------------------|--------------|.........
!      <---local col_ind---> <----halo---->
!
!#######################################################
!
!      <----"compressed whole vector"---->
!      <-------------len_vec------------->
!     |----------------------|------------|
!                             <-glb_addr->
!#######################################################

subroutine crs_comp_halo(CRS, row_start, row_end, mpi_comm_part)
  use mpi
  use mod_matvec
  use common_routines
  implicit none
  integer, intent(in) :: mpi_comm_part
  integer, intent(in) :: row_start(:), row_end(:)
  type(CRS_mat), intent(inout) :: CRS
  integer i, j, k, p_id, num_elems_halo, num_elems_halo_back, max_val, ind
  integer, allocatable :: sorted_order(:), new_order(:), si_temp(:)
  integer, allocatable :: li_temp(:), list(:)
  real(8), allocatable :: r_temp(:)
  integer :: me_proc, num_procs, proc, ierr, count_req, min_proc, max_proc
  integer, allocatable :: req(:), st_is(:, :)
  
  interface
     subroutine make_comm_from_list(list, row_start, row_end, num_elems, ptr, min_proc, max_proc, mpi_comm_part)
       integer, intent(in) :: list(:), row_start(:), row_end(:)
       integer, intent(out) :: num_elems(:), ptr(:), min_proc, max_proc
       integer, intent(in) :: mpi_comm_part
     end subroutine make_comm_from_list
  end interface
  
  call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1
  
  if(num_procs == 1) then
     call crs_comp_halo_seq_dummy(CRS)
     return
  endif

  max_val = 0
  do i = 1, CRS%n_row
     if(max_val < (CRS%row_ptr(i+1)-CRS%row_ptr(i))) max_val = CRS%row_ptr(i+1) - CRS%row_ptr(i)
  enddo
  
  allocate(li_temp(max_val))
  allocate(CRS%row_ptr_halo(CRS%n_row), CRS%col_ind(CRS%row_ptr(CRS%n_row+1)-1))
  allocate(r_temp(max_val))

  num_elems_halo = 0
  do i = 1, CRS%n_row !Calc each row_ptr and reod col_ind
     CRS%row_ptr_halo(i) = CRS%row_ptr(i+1)
     j = CRS%row_ptr(i)
     do while(j < CRS%row_ptr_halo(i))
        if(CRS%glb_col_ind(j) < row_start(me_proc) .or. row_end(me_proc) < CRS%glb_col_ind(j)) then !If the element is global
           num_elems_halo = num_elems_halo + 1
           CRS%row_ptr_halo(i) = CRS%row_ptr_halo(i) - 1
           ind = CRS%row_ptr(i+1)-j-1

           li_temp(1:ind) = CRS%glb_col_ind(j+1:CRS%row_ptr(i+1)-1)
           CRS%glb_col_ind(CRS%row_ptr(i+1)-1) = CRS%glb_col_ind(j)
           CRS%glb_col_ind(j:CRS%row_ptr(i+1)-2) =  li_temp(1:ind)

           r_temp(1:ind) = CRS%val(j+1:CRS%row_ptr(i+1)-1)
           CRS%val(CRS%row_ptr(i+1)-1) = CRS%val(j)
           CRS%val(j:CRS%row_ptr(i+1)-2) =  r_temp(1:ind)
        else !###############################################################################################If the element is local
           CRS%col_ind(j) = int(CRS%glb_col_ind(j) - row_start(me_proc) + 1, 4)
           j = j + 1
        endif
     enddo
  enddo
  deallocate(li_temp)
  if(allocated(r_temp)) then
     deallocate(r_temp)
  endif

  allocate(list(num_elems_halo))
  list = 0
  num_elems_halo_back = num_elems_halo
  num_elems_halo = 0
  do i = 1, CRS%n_row !Changing col_ind to compressed address
     do j = CRS%row_ptr_halo(i), CRS%row_ptr(i+1)-1

        k = 1
        do while(.true.)
           if(CRS%glb_col_ind(j) == list(k)) then
              CRS%col_ind(j) = k + CRS%n_row
              exit
           elseif(k > num_elems_halo) then
              num_elems_halo = num_elems_halo + 1
              list(num_elems_halo) = CRS%glb_col_ind(j)
              CRS%col_ind(j) = num_elems_halo + CRS%n_row
              exit
           endif
           k = k + 1
        enddo

     enddo
  enddo

  CRS%len_vec = CRS%n_row + num_elems_halo
  allocate(CRS%glb_addr(CRS%n_row+1:CRS%len_vec))

  CRS%glb_addr(CRS%n_row+1:CRS%len_vec) = list(1:num_elems_halo)
  deallocate(list)

  allocate(sorted_order(CRS%n_row+1:CRS%len_vec), new_order(CRS%n_row+1:CRS%len_vec))
  do i = CRS%n_row+1, CRS%len_vec !Sorting halo elements with glb_addr
     sorted_order(i) = i
  enddo

  call quicksort_nonval(CRS%glb_addr, sorted_order, num_elems_halo)
  do i = CRS%n_row+1, CRS%len_vec
     new_order(sorted_order(i)) = i
  enddo
  deallocate(sorted_order)

  do i = 1, CRS%n_row !Input new order
     do j = CRS%row_ptr_halo(i), CRS%row_ptr(i+1)-1
        CRS%col_ind(j) = new_order(CRS%col_ind(j))
     enddo
  enddo
  deallocate(new_order)

  !Making info for communication
  allocate(CRS%num_send(num_procs), CRS%num_recv(num_procs), CRS%ptr_send(num_procs), CRS%ptr_recv(num_procs))

  call make_comm_from_list(CRS%glb_addr, row_start, row_end, CRS%num_recv, CRS%ptr_recv, min_proc, max_proc, mpi_comm_part)
  ! write(*, *) 'Check range', me_proc, CRS%num_recv, CRS%ptr_recv
  ! write(*, *) 'Check point1', me_proc
  CRS%ptr_recv(:) = CRS%ptr_recv(:) + CRS%n_row
  call MPI_Alltoall(CRS%num_recv, 1, MPI_INTEGER, CRS%num_send, 1, MPI_INTEGER, mpi_comm_part, ierr)

  CRS%ptr_send(1) = 1
  CRS%num_send_all = sum(CRS%num_send)
  allocate(CRS%list_src(CRS%num_send_all))

  allocate(req(num_procs))
  count_req = 1
  do proc = 1, num_procs     
     if(CRS%num_send(proc) /= 0) then
        call MPI_iRecv(CRS%list_src(CRS%ptr_send(proc)), CRS%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, req(count_req), ierr)
        count_req = count_req + 1
     endif
     if(proc /= num_procs) CRS%ptr_send(proc+1) = CRS%ptr_send(proc) + CRS%num_send(proc)
  enddo

  allocate(si_temp(CRS%len_vec-CRS%n_row))
  do proc = min_proc, max_proc !Sending global address for communiate on matrix vector multiplications
     if(CRS%num_recv(proc) /= 0) then
        si_temp(1:CRS%num_recv(proc)) = &
           int(CRS%glb_addr(CRS%ptr_recv(proc):CRS%ptr_recv(proc)+CRS%num_recv(proc)-1) - row_start(proc) + 1, 4)
        call MPI_Send(si_temp, CRS%num_recv(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
     endif
  enddo

  allocate(st_is(MPI_STATUS_SIZE, count_req-1))
  call MPI_Waitall(count_req-1, req, st_is, ierr)

end subroutine crs_comp_halo

subroutine crs_comp_halo_seq_dummy(CRS)
  use mod_matvec
  implicit none
  type(CRS_mat), intent(inout) :: CRS
  integer n_row, n_val

  n_row = CRS%n_row
  n_val = CRS%row_ptr(n_row+1)-1
  allocate(CRS%row_ptr_halo(n_row), CRS%col_ind(n_val), CRS%glb_addr(0))
  CRS%row_ptr_halo(1:n_row) = CRS%row_ptr(2:n_row+1)
  CRS%col_ind = int(CRS%glb_col_ind, 4)
  CRS%len_vec = CRS%n_row
  CRS%num_send_all = 0
  allocate(CRS%num_send(1), CRS%num_recv(1), CRS%ptr_send(1), CRS%ptr_recv(1), CRS%list_src(0))
  CRS%num_send(1) = 0
  CRS%num_recv(1) = 0
  CRS%ptr_send(1) = 1
  CRS%ptr_recv(1) = 1

end subroutine crs_comp_halo_seq_dummy

! subroutine transf_crs_to_bcrs(matA, BCRS, rrange, mpi_comm_part) !This routine was work with a matrix which has unsorted columns, maybe.
!   use mod_matvec
!   use common_routines
!   implicit none
!   integer, intent(in) :: mpi_comm_part
!   type(CRS_mat), intent(in) :: matA
!   type(BCRS_mat), intent(inout) :: BCRS
!   type(row_sepa), intent(in) :: rrange
!   integer, allocatable :: ind_to_bid(:)
!   integer, allocatable :: list_bind(:), list_halo(:), list_halo_all(:)
!   integer i, j, k, p_id, ind, ind_col, ind_halo, bind_row, inb_ind_col, inb_ind_row
!   integer bind_dgn, ind_row_glb, ind_col_glb, bind_col_glb
!   integer count_bind, count_halo, count_halo_all
!   integer bsize_row, bsize_col
!   logical dummy
!   integer, allocatable :: i_temp(:), backup_list_src(:), backup_int(:)
!   integer :: me_proc, num_procs, proc, min_proc, max_proc, count_req, ierr
!   integer, allocatable :: req(:), st_is(:, :)

!   call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
!   call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
!   me_proc = me_proc + 1

!   bsize_row = BCRS%bsize_row
!   bsize_col = BCRS%bsize_col

!   allocate(BCRS%row_ptr(BCRS%n_belem_row+1), BCRS%row_ptr_halo(BCRS%n_belem_row), &
!      ind_to_bid(matA%row_ptr(matA%n_row+1)-1))
!   allocate(list_bind(BCRS%n_belem_row), list_halo(matA%len_vec - matA%n_row), list_halo_all(matA%len_vec - matA%n_row))
!   ind_to_bid = 0

!   BCRS%row_ptr(1) = 1

!   count_halo_all = 0
!   do k = 1, matA%n_row, bsize_row !Counting blocks on each row and making "list_halo_all"
!      bind_row = (k - mod(k-1, bsize_row)) / bsize_row + 1
!      bind_dgn = k + rrange%row_start(me_proc) + bsize_row - 2

!      count_bind = 0
!      count_halo = 0
!      do j = k, k+bsize_row-1 !Count number of blocks in bind_row-th block-row-index
!         ind_row_glb = j + rrange%row_start(me_proc) - 1
!         if(ind_row_glb > matA%n_row_glb) exit

!         do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1 !Local index
!            if(matA%glb_col_ind(i) <= bind_dgn) cycle !Pickup upper part. Column index of off-diag part is larger than "k+bsize_row-1"
!            bind_col_glb = (matA%glb_col_ind(i) - int(mod(matA%glb_col_ind(i)-1, int(bsize_row, 8)))) / int(bsize_row, 8) + 1
!            if(.not. bin_search(list_bind(1:count_bind), bind_col_glb, p_id)) then !Counting local elements on jth-row and making list "list_bind"
!               list_bind(p_id+1:count_bind+1) = list_bind(p_id:count_bind)
!               list_bind(p_id) = bind_col_glb
!               count_bind = count_bind + 1
!               ! if(me_proc == 1 .and. k == 1) write(*, *) 'Making local list', list_bind(1:count_bind), bind_col_glb
!            endif
!         enddo

!         do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1 !Halo index
!            if(matA%glb_col_ind(i) <= bind_dgn) cycle !Pickup upper part. Column index of off-diag part is larger than "k+bsize_row-1"
!            bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
!            if(.not. bin_search(list_halo(1:count_halo), bind_col_glb, p_id)) then !Counting halo elements on jth-row and making list "list_halo"
!               list_halo(p_id+1:count_halo+1) = list_halo(p_id:count_halo)
!               list_halo(p_id) = bind_col_glb
!               count_halo = count_halo + 1
!               ! if(me_proc == 1 .and. k == 1) write(*, *) 'Making global list', list_halo(1:count_halo), bind_col_glb

!               if(.not. bin_search(list_halo_all(1:count_halo_all), bind_col_glb, p_id))then !Counting all halo elements and making list "list_halo_all"
!                  ! if(me_proc == 1) write(*, *) 'bin search', list_halo_all(1:count_halo_all), bind_col_glb, p_id
!                  list_halo_all(p_id+1:count_halo_all+1) = list_halo_all(p_id:count_halo_all)
!                  list_halo_all(p_id) = bind_col_glb
!                  count_halo_all = count_halo_all + 1
!                  ! if(me_proc == 1) write(*, *) 'Process', j, i, bind_col_glb, count_halo_all, list_halo_all(1:count_halo_all)
!               endif
!            endif
!         enddo

!      enddo

!      BCRS%row_ptr_halo(bind_row) = BCRS%row_ptr(bind_row) + count_bind
!      BCRS%row_ptr(bind_row+1) = BCRS%row_ptr_halo(bind_row) + count_halo

!      ! if(me_proc == 1 .and. k == 1) write(*, *) 'Check 1st local  list', list_bind(1:count_bind)
!      ! if(me_proc == 1 .and. k == 1) write(*, *) 'Check 1st global list', list_halo(1:count_halo)

!      do j = k, k+bsize_row-1 !Coordinate matA%val and block id
!         ind_row_glb = j + rrange%row_start(me_proc) - 1
!         if(ind_row_glb > matA%n_row_glb) exit
!         do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1
!            if(matA%glb_col_ind(i) < ind_row_glb) cycle
!            bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
!            if(bin_search(list_bind(1:count_bind), bind_col_glb, p_id)) then
!               ind_to_bid(i) = p_id + BCRS%row_ptr(bind_row) - 1
!            else
!               ind_to_bid(i) = 0
!            endif
!            ! if(me_proc == 1 .and. k == 1) write(*, *) 'Updated ind_to_bid local', i, ind_to_bid(i), bind_col_glb
!         enddo

!         do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1
!            if(matA%glb_col_ind(i) < ind_row_glb) cycle
!            bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
!            dummy = bin_search(list_halo(1:count_halo), bind_col_glb, p_id)
!            ind_to_bid(i) = p_id + BCRS%row_ptr_halo(bind_row) - 1
!            ! if(me_proc == 1 .and. k == 1) write(*, *) 'Updated ind_to_bid global', i, ind_to_bid(i), bind_col_glb
!         enddo
!      enddo

!   enddo
!   ! if(me_proc == 1) write(*, *) "ind_to_bind", ind_to_bid

!   allocate(BCRS%val(bsize_row, bsize_col, BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%dgn(bsize_row, bsize_col, BCRS%n_belem_row), &
!      BCRS%inv_dgn(bsize_row, bsize_col, BCRS%n_belem_row))
!   allocate(BCRS%col_ind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%col_bind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), &
!      BCRS%glb_col_ind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%glb_col_bind(BCRS%row_ptr(BCRS%n_belem_row+1)-1))

!   BCRS%glb_col_bind = -1

!   do j = 1, matA%n_row !Put val and col to BCRS
!      bind_row = (j - mod(j-1, bsize_row)) / bsize_row + 1!mc%row_bstart

!      if(mod(j, bsize_row) == 1) then
!         BCRS%dgn(:, :, bind_row) = zero !Initialization
!         do i = BCRS%row_ptr(bind_row), BCRS%row_ptr(bind_row+1)-1
!            BCRS%val(:, :, i) = zero
!            BCRS%col_ind(i) = 0
!         enddo
!      endif

!      ind_row_glb = j + rrange%row_start(me_proc) - 1
!      inb_ind_row = mod(j-1, bsize_row) + 1

!      do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1 !For local elements
!         if(matA%glb_col_ind(i) < ind_row_glb) cycle
!         inb_ind_col = int(mod(matA%glb_col_ind(i)-1, int(bsize_col, 8)), 4) + 1
!         if(ind_to_bid(i) == 0) then
!            BCRS%dgn(inb_ind_row, inb_ind_col, bind_row) = matA%val(i)
!         else
!            BCRS%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
!            if(BCRS%col_ind(ind_to_bid(i)) == 0) then
!               BCRS%col_ind(ind_to_bid(i)) = matA%col_ind(i) - inb_ind_col + 1
!               BCRS%col_bind(ind_to_bid(i)) = (BCRS%col_ind(ind_to_bid(i))-1) / bsize_col + 1
!               BCRS%glb_col_ind(ind_to_bid(i)) = matA%glb_col_ind(i) - inb_ind_col + 1
!               BCRS%glb_col_bind(ind_to_bid(i)) = (BCRS%glb_col_ind(ind_to_bid(i))-1) / bsize_col + 1
!            endif
!         endif
!         ! if(me_proc == 1 .and. ind_row_glb == 1) write(*, *) 'Referencing ind_to_bid local', i, ind_to_bid(i)
!      enddo

!      do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1 !For global elements
!         if(matA%glb_col_ind(i) < ind_row_glb) cycle

!         bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
!         if(.not. bin_search(list_halo_all(1:count_halo_all), bind_col_glb, ind_halo)) then
!            write(*, *) 'Error. There is no element ID', bind_col_glb, 'in the list_halo_all.', me_proc, j, i, matA%glb_col_ind(i)
!            stop
!         endif

!         inb_ind_col = int(mod(matA%glb_col_ind(i)-1, int(bsize_col, 8)), 4) + 1

!         BCRS%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
!         if(BCRS%col_ind(ind_to_bid(i)) == 0) then
!            BCRS%col_ind(ind_to_bid(i)) = (ind_halo+BCRS%n_belem_row-1) * bsize_col + 1
!            BCRS%col_bind(ind_to_bid(i)) = ind_halo+BCRS%n_belem_row
!            BCRS%glb_col_ind(ind_to_bid(i)) = matA%glb_col_ind(i) - inb_ind_col + 1
!            BCRS%glb_col_bind(ind_to_bid(i)) = (BCRS%glb_col_ind(ind_to_bid(i))-1) / bsize_col + 1
!         endif
!         ! if(me_proc == 1 .and. ind_row_glb == 1) write(*, *) 'Referencing ind_to_bid global', i, ind_to_bid(i)
!      enddo

!   enddo

!   if(me_proc == num_procs) then
!      do i = bsize_row-BCRS%padding+1, bsize_row  !Fill last row_block without any datas.
!         BCRS%dgn(i, 1:BCRS%bsize_col, BCRS%n_belem_row) = zero
!         BCRS%dgn(i, i, BCRS%n_belem_row) = one
!      enddo
!   endif

!   BCRS%n_row = BCRS%n_belem_row*bsize_row  !Making glb_addr
!   BCRS%len_vec_blk = BCRS%n_belem_row + count_halo_all
!   BCRS%len_vec = BCRS%len_vec_blk * bsize_col
!   ! write(*, *) 'Check len_vec', me_proc, BCRS%len_vec, BCRS%n_belem_row, count_halo_all
!   allocate(BCRS%glb_addr(BCRS%n_row+1:BCRS%len_vec), BCRS%glb_addr_blk(BCRS%n_belem_row+1:BCRS%n_belem_row+count_halo_all))
!   ind = BCRS%n_belem_row * bsize_col + 1
!   do j = 1, count_halo_all
!      ind_col_glb = (list_halo_all(j)-1) * bsize_col
!      do i = 1, bsize_col
!         BCRS%glb_addr(ind) = ind_col_glb + i
!         ind = ind + 1
!      enddo
!      BCRS%glb_addr_blk(BCRS%n_belem_row + j) = list_halo_all(j)
!   enddo
!   ! if(me_proc == 1) write(*, *) 'Check', BCRS%glb_addr((BCRS%n_belem_row)*bsize_col+1:BCRS%len_vec)

!   allocate(BCRS%num_send_blk(num_procs), BCRS%num_recv_blk(num_procs), BCRS%ptr_send_blk(num_procs), BCRS%ptr_recv_blk(num_procs))

!   call make_comm_from_list(BCRS%glb_addr_blk, rrange%row_bstart, rrange%row_bend, BCRS%num_recv_blk, BCRS%ptr_recv_blk, min_proc, max_proc, mpi_comm_part)

!   BCRS%ptr_recv_blk(:) = BCRS%ptr_recv_blk(:) + BCRS%n_belem_row
!   call MPI_Alltoall(BCRS%num_recv_blk, 1, MPI_INTEGER, BCRS%num_send_blk, 1, MPI_INTEGER, mpi_comm_part, ierr)

!   BCRS%ptr_send_blk(1) = 1
!   BCRS%num_send_all_blk = sum(BCRS%num_send_blk)
!   ! allocate(BCRS%list_src(BCRS%num_send_all), BCRS%list_src_blk(BCRS%num_send_all_blk), req(num_procs))
!   allocate(BCRS%list_src_blk(BCRS%num_send_all_blk), req(num_procs))
!   count_req = 1
!   do proc = 1, num_procs     
!      if(BCRS%num_send_blk(proc) /= 0) then
!         call MPI_iRecv(BCRS%list_src_blk(BCRS%ptr_send_blk(proc)), BCRS%num_send_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, &
!            req(count_req), ierr)
!         count_req = count_req + 1
!      endif
!      if(proc /= num_procs) BCRS%ptr_send_blk(proc+1) = BCRS%ptr_send_blk(proc) + BCRS%num_send_blk(proc)
!   enddo

!   allocate(i_temp(BCRS%len_vec_blk-BCRS%n_belem_row))
!   do proc = min_proc, max_proc !Sending global address for communiate on matrix vector multiplications
!      if(BCRS%num_recv_blk(proc) /= 0) then
!         i_temp(1:BCRS%num_recv_blk(proc)) = &
!            int(BCRS%glb_addr_blk(BCRS%ptr_recv_blk(proc):BCRS%ptr_recv_blk(proc)+BCRS%num_recv_blk(proc)-1) - rrange%row_bstart(proc) + 1, 4)
!         call MPI_Send(i_temp, BCRS%num_recv_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
!      endif
!   enddo

!   allocate(st_is(MPI_STATUS_SIZE, count_req-1))
!   call MPI_Waitall(count_req-1, req, st_is, ierr)

!   allocate(BCRS%num_send(num_procs), BCRS%num_recv(num_procs), BCRS%ptr_send(num_procs), BCRS%ptr_recv(num_procs))
!   BCRS%num_send_all = BCRS%num_send_all_blk * BCRS%bsize_row
!   allocate(BCRS%list_src(BCRS%num_send_all_blk))

!   BCRS%num_send(:) = BCRS%num_send_blk(:) * BCRS%bsize_row
!   BCRS%num_recv(:) = BCRS%num_recv_blk(:) * BCRS%bsize_col
!   BCRS%ptr_send(:) = (BCRS%ptr_send_blk(:)-1) * BCRS%bsize_row + 1
!   BCRS%ptr_recv(:) = (BCRS%ptr_recv_blk(:)-1) * BCRS%bsize_col + 1
!   BCRS%list_src(:) = (BCRS%list_src_blk(:)-1) * BCRS%bsize_row + 1

! end subroutine transf_crs_to_bcrs

subroutine make_comm_from_list(list, row_start, row_end, num_elems, ptr, min_proc, max_proc, mpi_comm_part)
  use mpi
  implicit none
  integer, intent(in) :: list(:), row_start(:), row_end(:)
  integer, intent(out) :: num_elems(:), ptr(:), min_proc, max_proc
  integer, intent(in) :: mpi_comm_part
  integer i, j, k, num_list, count
  integer :: me_proc, num_procs, proc, ierr

  call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1

  num_list = size(list)

  num_elems(:) = 0
  if(num_list == 0) then
     min_proc = num_procs
     max_proc = 1
     ptr(:) = 0
     return
  endif

  proc = 1
  do while(.true.)
     if(list(1) <= row_end(proc)) then
        min_proc = proc
        exit
     end if
     proc = proc + 1
  enddo
  proc = 1
  do while(.true.)
     ! write(*, *) 'Check', read_buf(num_list)%row, rrange%row_end(proc)
     if(list(num_list) <= row_end(proc)) then
        max_proc = proc
        exit
     end if
     proc = proc + 1
  enddo

  if(min_proc == max_proc) then
     num_elems(min_proc) = num_list
  else
     proc = min_proc
     i = 1
     count = 0
     do while(i <= num_list) !Check number of data to send each proc
        ! if(read_buf(i)%row >= 1048576 .and. me_proc == 1) write(*, *) 'Check', i, proc, read_buf(i)%row, rrange%row_end(proc), count
        if(list(i) > row_end(proc)) then
           ! write(*, *) 'Check', me_proc, i, proc, count, read_buf(i)%row, rrange%row_end(proc), read_buf(i-1)%row
           num_elems(proc) = count
           proc = proc + 1
           count = - 1
           i = i - 1
           if(proc == max_proc) exit
        end if
        i = i + 1
        count = count + 1
     enddo
     num_elems(max_proc) = num_list - i
  endif

  ptr(1) = 1
  do proc = 2, num_procs
     ptr(proc) = ptr(proc-1) + num_elems(proc-1)
  enddo

end subroutine make_comm_from_list
