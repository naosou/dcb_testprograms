module common_routines

  interface
     recursive subroutine quicksort(master, slave, val, n)
       integer, intent(in) :: n
       integer, intent(inout) :: master(:), slave(:)
       real(8), intent(inout) :: val(:)
     end subroutine quicksort

     recursive subroutine quicksort_nonval(master, slave, n)
       integer,     intent(in) :: n
       integer, intent(inout) :: master(:)
       integer,    intent(inout) ::slave(:)
     end subroutine quicksort_nonval
  end interface

end module common_routines

recursive subroutine quicksort(master, slave, val, n)
  implicit none
  integer, intent(in) :: n
  integer, intent(inout) :: master(:), slave(:)
  real(8), intent(inout) :: val(:)
  integer i, j
  integer pivot, temp_i
  real(8) temp_r
  logical flag

  if(n <= 1) return

  flag = .false.
  temp_i = master(1)
  do i = 2, n
     if(temp_i /= master(i)) then
        flag = .true.
        exit
     endif
  enddo

  if(.not. flag) return

  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif

  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo

     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo

     if (i > j) exit

     temp_i = master(i)
     master(i) = master(j)
     master(j) = temp_i

     temp_i = slave(i)
     slave(i) = slave(j)
     slave(j) = temp_i

     temp_r = val(i)
     val(i) = val(j)
     val(j) = temp_r
     
     i = i + 1
     j = j - 1

  enddo

  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
  if (n - 1 + 1 >= 2) call quicksort(master(i:n), slave(i:n), val(i:n), n - i + 1) 
  
end subroutine quicksort

recursive subroutine quicksort_nonval(master, slave, n)
  implicit none
  integer,     intent(in) :: n
  integer, intent(inout) :: master(:)
  integer,    intent(inout) ::slave(:)
  integer i, j
  integer pivot, temp_li
  integer temp_si
  logical flag

  if(n <= 1) return
  
  flag = .false.
  temp_li = master(1)
  do i = 2, n
     if(temp_li /= master(i)) then
        flag = .true.
        exit
     endif
  enddo
  
  if(.not. flag) return
  
  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif
  
  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo
     
     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo
     
     if (i > j) exit
     
     temp_li = master(i)
     master(i) = master(j)
     master(j) = temp_li
     
     temp_si = slave(i)
     slave(i) = slave(j)
     slave(j) = temp_si
     
     i = i + 1
     j = j - 1
     
  enddo
  
  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort_nonval(master(1:i-1), slave(1:i-1), i-1)
  if (n - 1 + 1 >= 2) call quicksort_nonval(master(i:n), slave(i:n), n - i + 1) 
  
end subroutine quicksort_nonval
