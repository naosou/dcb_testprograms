#!/bin/sh

#------ pjsub option -------- #
#PJM -L rscgrp=regular-cache
#PJM -L node=28
#PJM --mpi proc=400
#PJM --omp thread=4
#PJM -L elapse=1:00:00
#PJM -g jh170017
#PJM -j
####PJM -o out9

#------- Program execution ------- #
source /usr/local/bin/hybrid_core_setting.sh 2
###export I_MPI_ADJUST_ALLREDUCE=8
export I_MPI_ADJUST_ALLREDUCE="12:1-4;11:5-8192;3:8193-16384;2:16385-262144;8:262145-10001048576"
export OMP_NUM_THREADS=4
#mpiexec.hydra -n 420 ./lhbiem lhtest.in
#mpiexec.hydra -n 144 ./lhbiem lhtest.in
#mpiexec.hydra -n 225 ./lhbiem lhtest.in
#mpiexec.hydra -n 9 ./lhbiem lhtest.in
#mpiexec.hydra -n 4 ./lhbiem lhtest.in
#mpiexec.hydra -n 1 ./lhbiem lhtest.in
#mpiexec.hydra -n 49 ./lhbiem lhtest.in
#mpiexec.hydra -n 64 ./lhbiem lhtest.in
#mpiexec.hydra -n 81 ./lhbiem lhtest.in
#mpiexec.hydra -n 100 ./lhbiem lhtest.in
#mpiexec.hydra -n 144 ./lhbiem lhtest.in
#mpiexec.hydra -n 196 ./lhbiem lhtest.in
#mpiexec.hydra -n 256 ./lhbiem lhtest.in
#mpiexec.hydra -n 324 ./lhbiem lhtest.in
mpiexec.hydra -n 400 ./lhbiem lhtest.in
#mpiexec.hydra -n 625 ./lhbiem lhtest.in
#mpiexec.hydra -n 900 ./lhbiem lhtest.in
